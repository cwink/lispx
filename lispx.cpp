#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "include/lispx.hpp"

#include <iostream>

SCENARIO("When testing a simple lisp expression, the results are correct.", "[simple_expression]") {
  GIVEN("A simple expression and standard environment.") {
    constexpr auto program = R"((begin (define r 10) (* pi (* r r))))";
    auto environment = lispx::standard_environment();

    WHEN("It is evaluated.") {
      auto evaluated = lispx::evaluate_string(program, environment);

      THEN("The result is correct.") { REQUIRE(evaluated == lispx::Object(314.159265)); }
    }
  }
}

SCENARIO("When testing a lisp expression that creates a function and calls it, the results are correct and the scope "
         "is respected.",
         "[function_call_scope]") {
  GIVEN("An expression, standard environment, and two call expressions.") {
    constexpr auto program1 = R"((define r 10.0))";
    constexpr auto program2 = R"((define circle-area (lambda (r) (* pi (* r r)))))";
    constexpr auto program3 = R"((circle-area 3.0))";
    constexpr auto program4 = R"(r)";
    auto environment = lispx::standard_environment();

    WHEN("It is evaluated.") {
      auto evaluated1 = lispx::evaluate_string(program1, environment);
      auto evaluated2 = lispx::evaluate_string(program2, environment);
      auto evaluated3 = lispx::evaluate_string(program3, environment);
      auto evaluated4 = lispx::evaluate_string(program4, environment);

      THEN("The result is correct.") {
        REQUIRE(evaluated1 == lispx::Object(1.0));
        REQUIRE(evaluated2 == lispx::Object(1.0));
        REQUIRE(evaluated3 == lispx::Object(28.274333877));
        REQUIRE(evaluated4 == lispx::Object(10.0));
      }
    }
  }
}

SCENARIO("When testing a more advanced lisp expression, the results are correct.", "[advanced_expression]") {
  GIVEN("An advanced expression and standard environment.") {
    constexpr auto program1 = R"((define fact (lambda (n) (if (<= n 1) 1 (* n (fact (- n 1)))))))";
    constexpr auto program2 = R"((fact 10))";
    constexpr auto program3 = R"((define circle-area (lambda (r) (* pi (* r r)))))";
    constexpr auto program4 = R"((circle-area (fact 10)))";
    auto environment = lispx::standard_environment();

    WHEN("It is evaluated.") {
      auto evaluated1 = lispx::evaluate_string(program1, environment);
      auto evaluated2 = lispx::evaluate_string(program2, environment);
      auto evaluated3 = lispx::evaluate_string(program3, environment);
      auto evaluated4 = lispx::evaluate_string(program4, environment);

      THEN("The result is correct.") {
        REQUIRE(evaluated1 == lispx::Object(1.0));
        REQUIRE(evaluated2 == lispx::Object(3628800.0));
        REQUIRE(evaluated3 == lispx::Object(1.0));
        REQUIRE(evaluated4 == lispx::Object(41369087205785.414062));
      }
    }
  }
}

SCENARIO("When testing aliasing of functions and a count function, the results are correct.", "[aliasing_count]") {
  GIVEN("An aliasing expression, a count function, and standard environment.") {
    constexpr auto program1 = R"((define first car))";
    constexpr auto program2 = R"((define rest cdr))";
    constexpr auto program3 =
        R"((define count (lambda (item L) (if L (+ (= item (first L)) (count item (rest L))) 0))))";
    constexpr auto program4 = R"((count 0 (list 0 1 2 3 0 0)))";
    constexpr auto program5 = R"((count (quote the) (quote (the more the merrier the bigger the better))))";
    constexpr auto program6 =
        R"((count (quote "the") (quote ("the" "more" "the" "merrier" "the" "bigger" "the" "better"))))";
    auto environment = lispx::standard_environment();

    WHEN("It is evaluated.") {
      auto evaluated1 = lispx::evaluate_string(program1, environment);
      auto evaluated2 = lispx::evaluate_string(program2, environment);
      auto evaluated3 = lispx::evaluate_string(program3, environment);
      auto evaluated4 = lispx::evaluate_string(program4, environment);
      auto evaluated5 = lispx::evaluate_string(program5, environment);
      auto evaluated6 = lispx::evaluate_string(program6, environment);

      THEN("The result is correct.") {
        REQUIRE(evaluated1 == lispx::Object(1.0));
        REQUIRE(evaluated2 == lispx::Object(1.0));
        REQUIRE(evaluated3 == lispx::Object(1.0));
        REQUIRE(evaluated4 == lispx::Object(3.0));
        REQUIRE(evaluated5 == lispx::Object(4.0));
        REQUIRE(evaluated6 == lispx::Object(4.0));
      }
    }
  }
}

SCENARIO("When testing advanced recursion, the results are correct.", "[advanced_recursion]") {
  GIVEN("Some expressions and a standard environment.") {
    constexpr auto program1 = R"((define twice (lambda (x) (* 2 x))))";
    constexpr auto program2 = R"((twice 5))";
    constexpr auto program3 = R"((define repeat (lambda (f) (lambda (x) (f (f x))))))";
    constexpr auto program4 = R"(((repeat twice) 10))";
    constexpr auto program5 = R"(((repeat (repeat (repeat twice))) 10))";
    constexpr auto program6 = R"(((repeat (repeat (repeat (repeat twice)))) 10))";
    auto environment = lispx::standard_environment();

    WHEN("It is evaluated.") {
      auto evaluated1 = lispx::evaluate_string(program1, environment);
      auto evaluated2 = lispx::evaluate_string(program2, environment);
      auto evaluated3 = lispx::evaluate_string(program3, environment);
      auto evaluated4 = lispx::evaluate_string(program4, environment);
      auto evaluated5 = lispx::evaluate_string(program5, environment);
      auto evaluated6 = lispx::evaluate_string(program6, environment);

      THEN("The result is correct.") {
        REQUIRE(evaluated1 == lispx::Object(1.0));
        REQUIRE(evaluated2 == lispx::Object(10.0));
        REQUIRE(evaluated3 == lispx::Object(1.0));
        REQUIRE(evaluated4 == lispx::Object(40.0));
        REQUIRE(evaluated5 == lispx::Object(2560.0));
        REQUIRE(evaluated6 == lispx::Object(655360.0));
      }
    }
  }
}

SCENARIO("When testing the fibonacci sequence function, a range function, and a map function, the results are correct.",
         "[fib_range_map]") {
  GIVEN("Some expressions and a standard environment.") {
    constexpr auto program1 = R"((define fib (lambda (n) (if (< n 2) 1 (+ (fib (- n 1)) (fib (- n 2)))))))";
    constexpr auto program2 = R"((define range (lambda (a b) (if (= a b) (quote ()) (cons a (range (+ a 1) b))))))";
    constexpr auto program3 = R"((range 0 10))";
    constexpr auto program4 = R"((map fib (range 0 10)))";
    auto environment = lispx::standard_environment();

    WHEN("It is evaluated.") {
      auto evaluated1 = lispx::evaluate_string(program1, environment);
      auto evaluated2 = lispx::evaluate_string(program2, environment);
      auto evaluated3 = lispx::evaluate_string(program3, environment);
      auto evaluated4 = lispx::evaluate_string(program4, environment);

      THEN("The result is correct.") {
        REQUIRE(evaluated1 == lispx::Object(1.0));
        REQUIRE(evaluated2 == lispx::Object(1.0));
        REQUIRE(lispx::string(evaluated3) ==
                "(0.000000 1.000000 2.000000 3.000000 4.000000 5.000000 6.000000 7.000000 8.000000 9.000000)");
        REQUIRE(lispx::string(evaluated4) ==
                "(1.000000 1.000000 2.000000 3.000000 5.000000 8.000000 13.000000 21.000000 34.000000 55.000000)");
      }
    }
  }
}

SCENARIO("When testing a simple lisp expression with newline characters and comments, the results are correct.",
         "[simple_expression_newline_comments]") {
  GIVEN("A simple expression and standard environment.") {
    constexpr auto program = R"((begin
                                  (define r 10) ; Define r to be 10.
                                  (* pi (* r r))))";

    auto environment = lispx::standard_environment();

    WHEN("It is evaluated.") {
      auto evaluated = lispx::evaluate_string(program, environment);

      THEN("The result is correct.") { REQUIRE(evaluated == lispx::Object(314.159265)); }
    }
  }
}

SCENARIO("When testing parenthesis failures, the results are correct.", "[parenthesis_failure]") {
  GIVEN("A few failures and a standard environment") {
    constexpr auto program1 = R"((+ 4 (- 2 1))";
    constexpr auto program2 = R"(+ 4 (- 2 1)))";
    constexpr auto program3 = R"(+ 4 (- 2 1) )";

    auto environment = lispx::standard_environment();
    WHEN("They are evaluated.") {
      auto failure1 = true;
      auto failure2 = true;
      auto failure3 = true;

      try {
        auto evaluated = lispx::evaluate_string(program1, environment);
        failure1 = false;
      } catch (const std::exception &) {
      }

      try {
        auto evaluated = lispx::evaluate_string(program2, environment);
        failure2 = false;
      } catch (const std::exception &) {
      }

      try {
        auto evaluated = lispx::evaluate_string(program3, environment);
        failure3 = false;
        std::cout << string(evaluated) << std::endl;
      } catch (const std::exception &) {
      }

      THEN("The results are correct.") {
        REQUIRE(failure1 == true);
        REQUIRE(failure2 == true);
        REQUIRE(failure3 == true);
      }
    }
  }
}

SCENARIO("When testing a simple calculator program (loading it from file), the results are correct.",
         "[calculator_file]") {
  GIVEN("A calculator program and standard environment.") {
    constexpr auto program1 = R"((calculator "+" 4.0 2.0))";
    constexpr auto program2 = R"((calculator "-" 4.0 2.0))";
    constexpr auto program3 = R"((calculator "*" 4.0 2.0))";
    constexpr auto program4 = R"((calculator "/" 4.0 2.0))";
    constexpr auto program5 = R"((calculator "&" 4.0 2.0))";

    auto environment = lispx::standard_environment();

    WHEN("It is evaluated.") {
      auto evaluated1 = lispx::evaluate_file("assets/calculator.lisp", environment);
      auto evaluated2 = lispx::evaluate_string(program1, environment);
      auto evaluated3 = lispx::evaluate_string(program2, environment);
      auto evaluated4 = lispx::evaluate_string(program3, environment);
      auto evaluated5 = lispx::evaluate_string(program4, environment);
      auto evaluated6 = lispx::evaluate_string(program5, environment);

      THEN("The result is correct.") {
        REQUIRE(evaluated1 == lispx::Object(1.0));
        REQUIRE(evaluated2 == lispx::Object(6.0));
        REQUIRE(evaluated3 == lispx::Object(2.0));
        REQUIRE(evaluated4 == lispx::Object(8.0));
        REQUIRE(evaluated5 == lispx::Object(2.0));
        REQUIRE(evaluated6 == lispx::Object(lispx::List()));
      }
    }
  }
}

SCENARIO("When testing the filter function, the results are correct.", "[filter]") {
  GIVEN("A filter expression and a standard environment.") {
    constexpr auto program = R"((filter (lambda (x) (< x 0)) (quote (-5 -4 -3 -2 -1 0 1 2 3 4 5))))";

    auto environment = lispx::standard_environment();

    WHEN("It is evaluated.") {
      auto evaluated = lispx::evaluate_string(program, environment);

      THEN("The result is correct.") {
        REQUIRE(lispx::string(evaluated) == "(-5.000000 -4.000000 -3.000000 -2.000000 -1.000000)");
      }
    }
  }
}
