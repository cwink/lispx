(define calculator
  (lambda (op left right)
    (if (= op "+") (+ left right)
    (if (= op "-") (- left right)
    (if (= op "*") (* left right)
    (if (= op "/") (/ left right) (quote ())))))))
