#include "../include/lispx.hpp"

#include <cmath>
#include <fstream>
#include <iostream>
#include <list>
#include <sstream>

namespace {
using Token = std::string;
using Tokens = std::list<Token>;

auto tokenize(const std::string &string) -> Tokens {
  auto tokens = Tokens();
  auto left_count{0};
  auto right_count{0};

  for (auto it = std::cbegin(string); it != std::cend(string); ++it) {
    while (std::isspace(*it) != 0) {
      ++it;
    }

    if (it == std::cend(string)) {
      break;
    }

    switch (*it) {
    case ';':
      while (*it != '\n' && it != std::cend(string)) {
        ++it;
      }

      continue;
    case '"': {
      auto stream = std::ostringstream();

      stream << *it;

      ++it;

      if (it == std::cend(string)) {
        throw lispx::Error("Unexpected end of file at '" + std::string(1, *it) + "' while tokenizing string.");
      }

      for (; *it != '"'; ++it) {
        if (*it == '\\') {
          ++it;

          if (it == std::cend(string)) {
            throw lispx::Error("Unexpected end of file at '" + std::string(1, *it) + "' while tokenizing string.");
          }

          switch (*it) {
          case 'n':
            stream << '\n';
            continue;
          case 't':
            stream << '\t';
            continue;
          }

          throw lispx::Error("Unknown escape character '\\" + std::string(1, *it) + "'.");
        }

        stream << *it;
      }

      stream << *it;

      if (it == std::cend(string)) {
        break;
      }

      tokens.emplace_back(stream.str());

      continue;
    }
    case '(':
      ++left_count;

      tokens.emplace_back("(");

      continue;
    case ')':
      ++right_count;

      tokens.emplace_back(")");

      continue;
    }

    auto stream = std::ostringstream();

    for (;
         it != std::cend(string) && (std::isalnum(*it) != 0 || *it == '+' || *it == '-' || *it == '*' || *it == '/' ||
                                     *it == '%' || *it == '<' || *it == '>' || *it == '=' || *it == '#' || *it == '.');
         ++it) {
      stream << *it;
    }

    if (stream.str().empty()) {
      throw lispx::Error("Invalid character '" + std::string(1, *it) + "' encountered during tokenization.");
    }

    --it;

    tokens.emplace_back(stream.str());
  }

  if (left_count != right_count) {
    throw lispx::Error("The number of left parenthesis does not match the number of right parenthesis.");
  }

  if (!tokens.empty() && tokens.front() != "(" && tokens.size() != 1) {
    throw lispx::Error("Expected only one token for '" + tokens.front() + "'.");
  }

  return tokens;
}

auto parse(Tokens &tokens) -> lispx::Object {
  if (tokens.empty()) {
    throw lispx::Error("Unexpected end of file when processing tokens.");
  }

  const auto token = tokens.front();
  tokens.pop_front();

  if (token == "(") {
    auto list = lispx::List();

    while (tokens.front() != ")") {
      list.emplace_back(parse(tokens));
    }

    tokens.pop_front();

    return lispx::Object(list);
  }

  if (token == ")") {
    throw lispx::Error("Unexpected right parenthesis found when processing tokens.");
  }

  if (token == "#t") {
    return lispx::Object(1.0);
  }

  if (token == "#f") {
    return lispx::Object(0.0);
  }

  if (!token.empty() && token.front() == '"' && token.back() == '"') {
    return lispx::Object(std::string(std::cbegin(token) + 1, std::cend(token) - 1), false);
  }

  try {
    return lispx::Object(std::stod(token));
  } catch (const std::exception &) {
    return lispx::Object(token, true);
  }
}
} // namespace

namespace lispx {
Object::Object(const Symbol &symbol, const bool is_symbol) noexcept
    : symbol_(is_symbol ? symbol : ""),
      string_(is_symbol ? "" : symbol), type_{is_symbol ? Object_type::symbol : Object_type::string} {}

Object::Object(Symbol &&symbol, const bool is_symbol) noexcept
    : symbol_(is_symbol ? std::move(symbol) : ""),
      string_(is_symbol ? "" : std::move(symbol)), type_{is_symbol ? Object_type::symbol : Object_type::string} {}

Object::Object(const Number number) noexcept : number_{number}, type_{Object_type::number} {}

Object::Object(const List &list) noexcept : list_(list), type_{Object_type::list} {}

Object::Object(List &&list) noexcept : list_(std::move(list)), type_{Object_type::list} {}

Object::Object(const Function &function) noexcept : function_(function), type_{Object_type::function} {}

Object::Object(Function &&function) noexcept : function_(std::move(function)), type_{Object_type::function} {}

auto Object::type() const noexcept -> Object_type { return type_; }

auto Object::symbol() const -> const Symbol & {
  if (type_ != Object_type::symbol) {
    throw Error("Object is not of type symbol.");
  }

  return symbol_;
}

auto Object::symbol() -> Symbol & {
  if (type_ != Object_type::symbol) {
    throw Error("Object is not of type symbol.");
  }

  return symbol_;
}

auto Object::number() const -> Number {
  if (type_ != Object_type::number) {
    throw Error("Object is not of type number.");
  }

  return number_;
}

auto Object::string() const -> const String & {
  if (type_ != Object_type::string) {
    throw Error("Object is not of type string.");
  }

  return string_;
}

auto Object::string() -> String & {
  if (type_ != Object_type::string) {
    throw Error("Object is not of type string.");
  }

  return string_;
}

auto Object::list() const -> const List & {
  if (type_ != Object_type::list) {
    throw Error("Object is not of type list.");
  }

  return list_;
}

auto Object::list() -> List & {
  if (type_ != Object_type::list) {
    throw Error("Object is not of type list.");
  }

  return list_;
}

auto Object::function() const -> const Function & {
  if (type_ != Object_type::function) {
    throw Error("Object is not of type function.");
  }

  return function_;
}

auto Object::function() -> Function & {
  if (type_ != Object_type::function) {
    throw Error("Object is not of type function.");
  }

  return function_;
}

auto Object::symbol(const Symbol &symbol) -> void {
  if (type_ != Object_type::symbol) {
    throw Error("Object is not of type symbol.");
  }

  symbol_ = symbol;
}

auto Object::symbol(Symbol &&symbol) -> void {
  if (type_ != Object_type::symbol) {
    throw Error("Object is not of type symbol.");
  }

  symbol_ = std::move(symbol);
}

auto Object::number(const Number number) -> void {
  if (type_ != Object_type::number) {
    throw Error("Object is not of type number.");
  }

  number_ = number;
}

auto Object::string(const String &string) -> void {
  if (type_ != Object_type::string) {
    throw Error("Object is not of type string.");
  }

  string_ = string;
}

auto Object::string(String &&string) -> void {
  if (type_ != Object_type::string) {
    throw Error("Object is not of type string.");
  }

  string_ = std::move(string);
}

auto Object::list(const List &list) -> void {
  if (type_ != Object_type::list) {
    throw Error("Object is not of type list.");
  }

  list_ = list;
}

auto Object::list(List &&list) -> void {
  if (type_ != Object_type::list) {
    throw Error("Object is not of type list.");
  }

  list_ = std::move(list);
}

auto Object::function(const Function &function) -> void {
  if (type_ != Object_type::function) {
    throw Error("Object is not of type function.");
  }

  function_ = function;
}

auto Object::function(Function &&function) -> void {
  if (type_ != Object_type::function) {
    throw Error("Object is not of type function.");
  }

  function_ = std::move(function);
}

auto operator==(const Object &left, const Object &right) -> bool {
  if (left.type() != right.type()) {
    return false;
  }

  switch (left.type()) {
  case Object_type::symbol:
    return left.symbol() == right.symbol();
  case Object_type::number:
    return std::fabs(left.number() - right.number()) < 0.00001;
  case Object_type::string:
    return left.string() == right.string();
  case Object_type::list:
    return left.list() == right.list();
  case Object_type::function:
  case Object_type::null:
    break;
  }

  return true;
}

auto operator!=(const Object &left, const Object &right) -> bool { return !(left == right); }

Environment_impl::Environment_impl(const Environment_map &map, const Environment &environment) noexcept
    : parent_(environment), map_(map) {}

Environment_impl::Environment_impl(Environment_map &&map, const Environment &environment) noexcept
    : parent_(environment), map_(std::move(map)) {}

auto Environment_impl::has(const Symbol &key) noexcept -> bool {
  if (map_.find(key) != std::cend(map_)) {
    return true;
  }

  if (parent_ != nullptr) {
    return parent_->has(key);
  }

  return false;
}

auto Environment_impl::operator[](const Symbol &key) noexcept -> Object & {
  if (map_.find(key) != std::cend(map_)) {
    return map_[key];
  }

  if (parent_ != nullptr) {
    return (*parent_)[key];
  }

  return map_[key];
}

auto string(const Object &object) noexcept -> std::string {
  switch (object.type()) {
  case Object_type::symbol:
    return object.symbol();
  case Object_type::number:
    return std::to_string(object.number());
  case Object_type::string:
    return "\"" + object.string() + "\"";
  case Object_type::list: {
    auto string_stream = std::ostringstream();

    string_stream << '(';

    if (!object.list().empty()) {
      auto it = std::cbegin(object.list());

      string_stream << string(*it);

      ++it;

      for (; it != std::cend(object.list()); ++it) {
        string_stream << ' ' << string(*it);
      }
    }

    string_stream << ')';

    return string_stream.str();
  }
  case Object_type::function:
    return "function";
  case Object_type::null:
    return "null";
  }
}

auto evaluate(const Object &object, const Environment &environment) -> Object {
  switch (object.type()) {
  case Object_type::symbol:
    if (!(*environment).has(object.symbol())) {
      throw Error("Can not find symbol '" + object.symbol() + "' in environment.");
    }

    return (*environment)[object.symbol()];
  case Object_type::number:
  case Object_type::string:
    return object;
  case Object_type::list: {
    auto item =
        object.list()[0].type() == Object_type::list ? evaluate(object.list()[0], environment) : object.list()[0];

    if (item.type() == Object_type::symbol) {
      if (item.symbol() == "begin") {
        if (object.list().size() < 2) {
          throw Error("Begin takes at least one argument.");
        }

        auto result = Object();

        for (auto it = std::cbegin(object.list()) + 1; it != std::cend(object.list()); ++it) {
          result = evaluate(*it, environment);
        }

        return result;
      }

      if (item.symbol() == "quote") {
        if (object.list().size() != 2) {
          throw Error("Quote supports only one argument.");
        }

        return object.list()[1];
      }

      if (item.symbol() == "if") {
        if (object.list().size() != 4) {
          throw Error("If supports only three arguments.");
        }

        auto condition = evaluate(object.list()[1], environment);

        auto result = ((condition.type() == Object_type::number && condition == Object(1.0)) ||
                       (condition.type() == Object_type::list && !condition.list().empty()))
                          ? object.list()[2]
                          : object.list()[3];

        return evaluate(result, environment);
      }

      if (item.symbol() == "define") {
        if (object.list().size() != 3) {
          throw Error("Define supports only two arguments.");
        }

        if (object.list()[1].type() != Object_type::symbol) {
          throw Error("Define's first argument must be a symbol.");
        }

        (*environment)[object.list()[1].symbol()] = evaluate(object.list()[2], environment);

        return Object(1.0);
      }

      if (item.symbol() == "lambda") {
        if (object.list().size() != 3) {
          throw Error("Lambda supports only two arguments.");
        }

        if (object.list()[1].type() != Object_type::list) {
          throw Error("Lambda's first argument must be a list of parameters.");
        }

        if (!std::all_of(std::cbegin(object.list()[1].list()), std::cend(object.list()[1].list()),
                         [](const auto &obj) { return obj.type() == Object_type::symbol; })) {
          throw Error("Each parameters in the Lambda's first argument must be symbols.");
        }

        return Object([object, environment](const auto &arguments, const auto &) {
          if (arguments.size() != object.list()[1].list().size()) {
            throw Error("Number of arguments for Lambda does not match the number of parameters required.");
          }

          auto map = Environment_map();

          auto at = std::cbegin(arguments);

          for (auto it = std::cbegin(object.list()[1].list()); it != std::cend(object.list()[1].list()); ++it, ++at) {
            map[it->symbol()] = *at;
          }

          auto new_environment = std::make_shared<Environment_impl>(Environment_impl(map, environment));

          return evaluate(object.list()[2], new_environment);
        });
      }
    }

    auto arguments = List();

    for (auto it = std::cbegin(object.list()) + 1; it != std::cend(object.list()); ++it) {
      arguments.emplace_back(evaluate(*it, environment));
    }

    auto procedure = item.type() == Object_type::function ? item : evaluate(item, environment);

    return procedure.function()(arguments, environment);
  }
  case Object_type::function:
  case Object_type::null:
    return object;
  }
}

auto evaluate_string(const std::string &program, const Environment &environment) -> Object {
  auto tokens = tokenize(program);

  auto object = parse(tokens);

  return evaluate(object, environment);
}

auto evaluate_file(const std::string &file, const Environment &environment) -> Object {
  auto file_stream = std::ifstream(file);

  if (!file_stream) {
    throw Error("Failed to read file '" + file + "'.");
  }

  auto program_stream = std::ostringstream();

  program_stream << file_stream.rdbuf();

  file_stream.close();

  auto tokens = tokenize(program_stream.str());

  auto object = parse(tokens);

  return evaluate(object, environment);
}

auto standard_environment() noexcept -> Environment {
  auto environment = Environment_impl();

  environment["pi"] = Object(3.14159265359);

  environment["*"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 2) {
      throw Error("Multiplication only supports two arguments.");
    }

    if (arguments[0].type() != Object_type::number || arguments[1].type() != Object_type::number) {
      throw Error("Multiplication only supports numbers.");
    }

    return Object(arguments[0].number() * arguments[1].number());
  });

  environment["%"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 2) {
      throw Error("Modulus only supports two arguments.");
    }

    if (arguments[0].type() != Object_type::number || arguments[1].type() != Object_type::number) {
      throw Error("Modulus only supports numbers.");
    }

    return Object(
        static_cast<double>(static_cast<int>(arguments[0].number()) % static_cast<int>(arguments[1].number())));
  });

  environment["/"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 2) {
      throw Error("Division only supports two arguments.");
    }

    if (arguments[0].type() != Object_type::number || arguments[1].type() != Object_type::number) {
      throw Error("Division only supports numbers.");
    }

    return Object(arguments[0].number() / arguments[1].number());
  });

  environment["+"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 2) {
      throw Error("Addition only supports two arguments.");
    }

    if (arguments[0].type() != Object_type::number || arguments[1].type() != Object_type::number) {
      throw Error("Addition only supports numbers.");
    }

    return Object(arguments[0].number() + arguments[1].number());
  });

  environment["-"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 2) {
      throw Error("Subtraction only supports two arguments.");
    }

    if (arguments[0].type() != Object_type::number || arguments[1].type() != Object_type::number) {
      throw Error("Subtraction only supports numbers.");
    }

    return Object(arguments[0].number() - arguments[1].number());
  });

  environment["<"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 2) {
      throw Error("Less than comparison only supports two arguments.");
    }

    if (arguments[0].type() != Object_type::number || arguments[1].type() != Object_type::number) {
      throw Error("Less than comparison only supports numbers.");
    }

    return Object(arguments[0].number() < arguments[1].number() ? 1.0 : 0.0);
  });

  environment[">"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 2) {
      throw Error("Greater than comparison only supports two arguments.");
    }

    if (arguments[0].type() != Object_type::number || arguments[1].type() != Object_type::number) {
      throw Error("Greater than comparison only supports numbers.");
    }

    return Object(arguments[0].number() > arguments[1].number() ? 1.0 : 0.0);
  });

  environment["<="] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 2) {
      throw Error("Less than or equal to comparison only supports two arguments.");
    }

    if (arguments[0].type() != Object_type::number || arguments[1].type() != Object_type::number) {
      throw Error("Less than or equal to comparison only supports numbers.");
    }

    return Object(arguments[0].number() <= arguments[1].number() ? 1.0 : 0.0);
  });

  environment[">="] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 2) {
      throw Error("Greater than or equal to comparison only supports two arguments.");
    }

    if (arguments[0].type() != Object_type::number || arguments[1].type() != Object_type::number) {
      throw Error("Greater than or equal to comparison only supports numbers.");
    }

    return Object(arguments[0].number() >= arguments[1].number() ? 1.0 : 0.0);
  });

  environment["="] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 2) {
      throw Error("Equal comparison only supports two arguments.");
    }

    return Object(arguments[0] == arguments[1] ? 1.0 : 0.0);
  });

  environment["list"] = Object([](const auto &arguments, const auto &) { return Object(arguments); });

  environment["car"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 1) {
      throw Error("Car only supports one argument.");
    }

    if (arguments[0].type() != Object_type::list) {
      throw Error("Car argument must be a list.");
    }

    if (arguments[0].list().empty()) {
      throw Error("Car argument must have at least 1 element.");
    }

    return arguments[0].list()[0];
  });

  environment["cdr"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 1) {
      throw Error("Cdr only supports one argument.");
    }

    if (arguments[0].type() != Object_type::list) {
      throw Error("Cdr argument must be a list.");
    }

    if (arguments[0].list().empty()) {
      throw Error("Cdr argument must have at least 1 element.");
    }

    auto list = List();

    for (auto it = std::cbegin(arguments[0].list()) + 1; it != std::cend(arguments[0].list()); ++it) {
      list.emplace_back(*it);
    }

    return Object(list);
  });

  environment["pow"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 1) {
      throw Error("Power only supports one argument.");
    }

    if (arguments[0].type() != Object_type::number) {
      throw Error("Power only supports a number.");
    }

    return Object(arguments[0].number() * arguments[0].number());
  });

  environment["cons"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 2) {
      throw Error("Cons only supports two arguments.");
    }

    if (arguments[0].type() != Object_type::symbol && arguments[0].type() != Object_type::number) {
      throw Error("Cons first argument must be a symbol or a number.");
    }

    if (arguments[1].type() != Object_type::list) {
      throw Error("Cons second argument must be a list.");
    }

    auto list = List();

    list.emplace_back(arguments[0]);

    for (auto it = std::cbegin(arguments[1].list()); it != std::cend(arguments[1].list()); ++it) {
      list.emplace_back(*it);
    }

    return Object(list);
  });

  environment["map"] = Object([](const auto &arguments, const auto &env) {
    if (arguments.size() != 2) {
      throw Error("Map only supports two arguments.");
    }

    if (arguments[1].type() != Object_type::list) {
      throw Error("Map's second argument must be a list.");
    }

    auto procedure = evaluate(arguments[0], env);

    if (procedure.type() != Object_type::function) {
      throw Error("Map's first argument must be a function.");
    }

    auto list = List();

    for (const auto &object : arguments[1].list()) {
      list.emplace_back(procedure.function()(List(1, object), env));
    }

    return Object(list);
  });

  environment["filter"] = Object([](const auto &arguments, const auto &env) {
    if (arguments.size() != 2) {
      throw Error("Filter only supports two arguments.");
    }

    if (arguments[1].type() != Object_type::list) {
      throw Error("Filter's second argument must be a list.");
    }

    auto procedure = evaluate(arguments[0], env);

    if (procedure.type() != Object_type::function) {
      throw Error("Filter's first argument must be a function.");
    }

    auto list = List();

    for (const auto &object : arguments[1].list()) {
      auto result = procedure.function()(List(1, object), env);

      if (result == lispx::Object(1.0)) {
        list.emplace_back(object);
      }
    }

    return Object(list);
  });

  environment["display"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 1) {
      throw Error("Display only supports one argument.");
    }

    if (arguments[0].type() == Object_type::string) {
      std::cout << arguments[0].string();
    } else {
      std::cout << string(arguments[0]);
    }

    return Object(1.0);
  });

  environment["display-line"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 1) {
      throw Error("Display-line only supports one argument.");
    }

    if (arguments[0].type() == Object_type::string) {
      std::cout << arguments[0].string() << std::endl;
    } else {
      std::cout << string(arguments[0]) << std::endl;
    }

    return Object(1.0);
  });

  environment["power"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 1) {
      throw Error("Power takes only one argument.");
    }

    if (arguments[0].type() != Object_type::number) {
      throw Error("Power takes only a number argument.");
    }

    return Object(arguments[0].number() * arguments[0].number());
  });

  environment["abs"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 1) {
      throw Error("Abs takes only one argument.");
    }

    if (arguments[0].type() != Object_type::number) {
      throw Error("Abs takes only a number argument.");
    }

    return Object(std::fabs(arguments[0].number()));
  });

  environment["sin"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 1) {
      throw Error("Sin takes only one argument.");
    }

    if (arguments[0].type() != Object_type::number) {
      throw Error("Sin takes only a number argument.");
    }

    return Object(std::sin(arguments[0].number()));
  });

  environment["cos"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 1) {
      throw Error("Cos takes only one argument.");
    }

    if (arguments[0].type() != Object_type::number) {
      throw Error("Cos takes only a number argument.");
    }

    return Object(std::cos(arguments[0].number()));
  });

  environment["tan"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 1) {
      throw Error("Tan takes only one argument.");
    }

    if (arguments[0].type() != Object_type::number) {
      throw Error("Tan takes only a number argument.");
    }

    return Object(std::tan(arguments[0].number()));
  });

  environment["asin"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 1) {
      throw Error("Asin takes only one argument.");
    }

    if (arguments[0].type() != Object_type::number) {
      throw Error("Asin takes only a number argument.");
    }

    return Object(std::asin(arguments[0].number()));
  });

  environment["acos"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 1) {
      throw Error("Acos takes only one argument.");
    }

    if (arguments[0].type() != Object_type::number) {
      throw Error("Acos takes only a number argument.");
    }

    return Object(std::acos(arguments[0].number()));
  });

  environment["atan"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 1) {
      throw Error("Atan takes only one argument.");
    }

    if (arguments[0].type() != Object_type::number) {
      throw Error("Atan takes only a number argument.");
    }

    return Object(std::atan(arguments[0].number()));
  });

  environment["sqrt"] = Object([](const auto &arguments, const auto &) {
    if (arguments.size() != 1) {
      throw Error("Sqrt takes only one argument.");
    }

    if (arguments[0].type() != Object_type::number) {
      throw Error("Sqrt takes only a number argument.");
    }

    return Object(std::sqrt(arguments[0].number()));
  });

  return std::make_shared<Environment_impl>(environment);
}
} // namespace lispx
