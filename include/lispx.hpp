#ifndef LISPX_HPP
#define LISPX_HPP

#include <functional>
#include <memory>
#include <stdexcept>
#include <unordered_map>
#include <vector>

namespace lispx {
using Error = std::runtime_error;

using Symbol = std::string;
using Number = double;
using String = std::string;

class Object;

using List = std::vector<Object>;

class Environment_impl;
using Environment = std::shared_ptr<Environment_impl>;

using Function = std::function<Object(const List &, const Environment &)>;

enum class Object_type : std::uint8_t { symbol, number, string, list, function, null };

class Object final {
  Symbol symbol_ = "";
  Number number_{0.0};
  String string_ = "";
  List list_ = List();
  Function function_ = Function();
  Object_type type_{Object_type::null};
  [[maybe_unused]] char padding_[7]{0};

public:
  Object() = default;
  Object(const Object &) = default;
  Object(Object &&) noexcept = default;
  auto operator=(const Object &) -> Object & = default;
  auto operator=(Object &&) noexcept -> Object & = default;
  ~Object() noexcept = default;

  explicit Object(const Symbol &symbol, bool is_symbol) noexcept;
  explicit Object(Symbol &&symbol, bool is_symbol) noexcept;
  explicit Object(Number number) noexcept;
  explicit Object(const List &list) noexcept;
  explicit Object(List &&list) noexcept;
  explicit Object(const Function &function) noexcept;
  explicit Object(Function &&function) noexcept;

  auto type() const noexcept -> Object_type;
  auto symbol() const -> const Symbol &;
  auto symbol() -> Symbol &;
  auto number() const -> Number;
  auto string() const -> const String &;
  auto string() -> String &;
  auto list() const -> const List &;
  auto list() -> List &;
  auto function() const -> const Function &;
  auto function() -> Function &;

  auto symbol(const Symbol &symbol) -> void;
  auto symbol(Symbol &&symbol) -> void;
  auto number(Number number) -> void;
  auto string(const String &string) -> void;
  auto string(String &&string) -> void;
  auto list(const List &list) -> void;
  auto list(List &&list) -> void;
  auto function(const Function &function) -> void;
  auto function(Function &&function) -> void;
};

auto operator==(const Object &left, const Object &right) -> bool;
auto operator!=(const Object &left, const Object &right) -> bool;

using Environment_map = std::unordered_map<Symbol, Object>;

class Environment_impl final {
  Environment parent_ = Environment(nullptr);
  Environment_map map_ = Environment_map();

public:
  Environment_impl() noexcept = default;
  Environment_impl(const Environment_impl &) = default;
  Environment_impl(Environment_impl &&) noexcept = default;
  auto operator=(const Environment_impl &) -> Environment_impl & = default;
  auto operator=(Environment_impl &&) noexcept -> Environment_impl & = default;
  ~Environment_impl() noexcept = default;

  Environment_impl(const Environment_map &map, const Environment &environment) noexcept;
  Environment_impl(Environment_map &&map, const Environment &environment) noexcept;

  auto has(const Symbol &key) noexcept -> bool;
  auto operator[](const Symbol &key) noexcept -> Object &;
};

auto string(const Object &object) noexcept -> std::string;
auto evaluate(const Object &object, const Environment &environment) -> Object;
auto evaluate_string(const std::string &program, const Environment &environment) -> Object;
auto evaluate_file(const std::string &file, const Environment &environment) -> Object;
auto standard_environment() noexcept -> Environment;
} // namespace lispx

#endif // LISPX_HPP
